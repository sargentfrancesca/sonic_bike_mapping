from django.contrib import admin
from sonic_bike.models import Tag, ZoneTag, Event, Zone

admin.site.register(Tag)
admin.site.register(ZoneTag)
admin.site.register(Event)
admin.site.register(Zone)