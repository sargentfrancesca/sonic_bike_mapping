// Copyright (C) 2015 Francesca Sargent, Dave Griffiths
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

function main_map_init_anon (map, options) {
    //If anon
}

function main_map_init_view (map, options) {
	editing = false;
	$.ajax({
		type: 'get',
		url: "/json/" ,
		success: function(data){
			// console.log(JSON.stringify(data));
			var geoJsonLayer = L.geoJson(data).addTo(map);

			popup = new L.Popup();

			geoJsonLayer.eachLayer(function(layer) {
				var geo_colour = layer.feature.properties.color

				layer.on('click', function() {
					layer_id = layer.feature.id;


					if (layer.feature.name) {
						var name = layer.feature.name;
					} else {
						var name = "No Name Yet";
					}

					if (layer.feature.tags) {
						var all_tags = layer.feature.tags;
						var tag_names = []
						for (i = 0; i < all_tags.length; i++) {
							tag_names.push(all_tags[i][1]);
						}
					} else {
						var tag_names = ["No Tags Yet"]
					}	

					console.log(name);

					var bounds = layer.getBounds();
	                var popupContent = "<strong>"+name+"</strong><br/><a href='/zone/"+layer_id+"/'>Edit Zone</a><hr/><strong>Assigned Tags: </strong><br/>" + tag_names.join(",");
	                popup.setLatLng(bounds.getCenter());
	                popup.setContent(popupContent);
	                map.openPopup(popup);
				})

				if (geo_colour != 'none') {
				    var style = {
				        fillColor : geo_colour,
				        color: geo_colour
				    };

				    layer.setStyle(style);
				}
			});

		}
	});
    setup_map(map, options);
}

// can't pass arguments through leaflet so have to use a lovely global variable
// to store the current editing zone (ignored in viewing mode)
var editing_zone_id = false;

function set_editing_zone(zone_id) {
    editing_zone_id=zone_id;
}

function main_map_init_edit(map, options) {
	editing = true;

	$.ajax({
		type: 'get',
		url: "/json/" ,
		success: function(data){

            // loop over each zone
            data.features.forEach(function(zone) {

            	zone_id = zone.id
                // add the geometry for this zone
		        geoJsonLayer = L.geoJson(zone).addTo(map);

                geoJsonLayer.eachLayer(function(layer) {
			        var geo_colour = layer.feature.properties.color;


			        var existing_tags = layer.feature.tags

                    // check the zone id
                    if (zone.id==editing_zone_id) {
                        layer.editing.enable();
                        items = layer.toGeoJSON();
                        
						layer.on('edit', function() {
							items = layer.toGeoJSON();
						});


						if (layer.feature.tags) {
							$('input[type=checkbox]').each(function() {
									input_name = parseInt(this.name);

									for (var i = 0; i < existing_tags.length; i++) {
										tag_name = existing_tags[i][0]
										
										if (input_name === tag_name){
											$('input[name='+input_name+']').prop('checked', true);
										}
									}
								})
							} else {
								var tags = ["No Tags Yet"]
							}	
                    }

                   


			        if (geo_colour != 'none') {
				        var style = {
				            fillColor : geo_colour,
				            color: geo_colour
				        };

				        layer.setStyle(style);
			        }
		        });
            });
        }
    });

    setup_map(map, options);

}


function setup_map(map, options) {
	var drawnItems = new L.FeatureGroup();
	map.addLayer(drawnItems);

	var drawControl = new L.Control.Draw({
		draw : {
			polygon: {
			    shapeOptions: {
			        color: '#FF6060'
			    },
			    allowIntersection: false,
				drawError: {
					color: 'orange',
					timeout: 1000
				},
				showArea: true,
				metric: false
			},
			circle : false,
			polyline : false,
			marker: false,
			rectangle: {
			    shapeOptions: {
			        color: '#FF8244'
			    },
			},
		},
		edit: {
			featureGroup: drawnItems
		}
	});
	
	map.addControl(drawControl);


	map.on('draw:created', function (e) {
		var type = e.layerType,
			layer = e.layer;
		drawnItems.addLayer(layer);
		

	});



	$('.save_map').click(function(e) {
		saveShapes(drawnItems);
    });

    if(editing===true) {
		map.removeControl(drawControl);
    }


}

function saveShapes(layer) {
	if (editing==true) {
		var shapes = getEditedShapes(items);
		var shape_name = $('#name').val();
		shapes['name'] = shape_name
		
		
		var tags_in = []
		var tags_out = []

		$('input[type=checkbox]').each(function() {
			if (this.checked) {
				tags_in.push(this.name);
			} else {
				tags_out.push(this.name);
			}		
		});

		shapes['tags_in'] = tags_in;
		shapes['tags_out'] = tags_out;

		var shape_json = JSON.stringify([shapes]);
		console.log(shape_json);
	} else {
		var shapes = getShapes(layer);
		var shape_json = JSON.stringify(shapes)
	}
	

	// console.log("data sent=", shape_json);

	$.post( "/json-send/", { shapes: shape_json })
	.done(function( data ) {
        $('.shapes-saved').show(1000, function() {
        	window.location.replace("/");
        })
	}).fail(function() {
		$('.shapes-failed').show(1000, function() {
			$(this).hide(5000)
		})
	});

}

var getEditedShapes = function(shape_layer) {

	var shapes = shape_layer;

	return shapes
}

var getShapes = function(shape_layer) {

	var shapes = [];

	shape_layer.eachLayer(function(layer) {
		var co_ords_geo = layer.toGeoJSON();

		if (co_ords_geo.geometry.type !== "Point") {
			var fill_colour = layer._path.attributes.stroke.nodeValue;
			co_ords_geo.properties.fillColor = fill_colour;
			co_ords_geo.properties.color = fill_colour;
			shapes.push(co_ords_geo)
		} else {
			shapes.push(co_ords_geo)
		}

	});

	return shapes;
};


