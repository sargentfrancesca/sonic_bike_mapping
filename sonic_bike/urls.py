from django.conf import settings
from django.conf.urls import patterns, include, url
from django.conf.urls.static import static
from django.views.generic import TemplateView

from django.contrib import admin
from zones import views

admin.autodiscover()

urlpatterns = patterns(
    "",
    url(r"^$", 'zones.views.index', name='home'),
    url(r'^userpage/', 'zones.views.user_redr', name='user_redr'),
    url(r"^user/(?P<usr>\d+)/$", 'zones.views.user_map', name='user_map'),
    url(r"^map/(?P<usr>\d+)/$", 'zones.views.view_user_map', name='view_user_map'),
    url(r"^all/", 'zones.views.view_all_map', name='view_all_map'),
    url(r"^admin/", include(admin.site.urls)),
    url(r"^account/", include("account.urls")),
    url(r'^all-json/', 'zones.views.get_zone_json', name='json'),
    url(r'^json/(?P<usrid>\d+)/$', 'zones.views.get_zone_json_user', name='json_user'),
    url(r'^json-send/', 'zones.views.send_zone_json', name='send_json'),
    url(r'^zone/(?P<pk>\d+)/$', views.ZoneView.as_view(), name='zoneview'),
    url(r'^tag-form/', 'zones.views.tag_form', name='tag_form'),
    url(r'^all-bike-json/', 'zones.views.get_bike_json', name='bike-json'),
    url(r'^bike-json/(?P<user>\d+)/$', 'zones.views.get_bike_json_user', name='bike-json-user'),
    url(r'^delete/tag/(?P<number>\d+)/$', 'zones.views.delete_tag', name='delete_tag'),
    url(r'^delete/zone/(?P<number>\d+)/$', 'zones.views.delete_zone', name='delete_zone'),
    url(r'^location/(?P<a>.+)/(?P<b>.+)/(?P<c>.+)$', r'zones.views.location_session_data', name="send_location"),
    url(r'^all-zones/', 'zones.views.view_all_zones_list', name='view_all_zones_list'),
    url(r'^zones/(?P<usr>\d+)/$', 'zones.views.view_user_zones_list', name='view_user_zones_list'),
)

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
