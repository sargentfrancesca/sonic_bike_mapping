from django.contrib import admin
from .models import Tag, ZoneTag, Event, Zone
# Register your models here.

admin.site.register(Tag)
admin.site.register(ZoneTag)
admin.site.register(Event)
admin.site.register(Zone)
