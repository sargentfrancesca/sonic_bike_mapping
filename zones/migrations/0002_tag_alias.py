# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('zones', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='tag',
            name='alias',
            field=models.CharField(default='parameter', max_length=64),
            preserve_default=False,
        ),
    ]
