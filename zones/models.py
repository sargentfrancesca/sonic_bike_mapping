# Create your models here.
# models.py
from django.db import models
from django.contrib.gis.db import models as gismodels
from django.contrib.auth.models import User

class Tag(models.Model):
	id = models.AutoField(primary_key=True)
	name = models.CharField(max_length=64)
	alias = models.CharField(max_length=64)
	description = models.TextField()

	def __repr__ (self):
		return self.alias

	def __str__ (self):
		return self.alias

class Event(models.Model):
	id = models.AutoField(primary_key=True)
	name = models.CharField(max_length=64)
	event_date = models.DateField(auto_now=False, auto_now_add=False)
	center_lat = models.FloatField()
	center_long = models.FloatField()
	zoom_level = models.IntegerField()

	def __repr__(self):
		return '<Event %s>' % self.name

	def __str__(self):
		return self.name

	class Meta:
		managed = True

class Zone(gismodels.Model):
	id = models.AutoField(primary_key=True)
	name = models.CharField(max_length=64)
	geom = gismodels.GeometryCollectionField(spatial_index=True)
	colour = models.CharField(max_length=7)
	event = models.ForeignKey(Event, related_name='zones', null=True, blank=True, default = None)
	objects = gismodels.GeoManager()
	author = models.ForeignKey(User, related_name='zones', null=True, blank=True, default=None)

	def __repr__ (self):
		return '<Zone %s>' % self.name

	def __str__ (self):
		return self.name

	class Meta:
		managed = True

class ZoneTag(models.Model):
	id = models.AutoField(primary_key=True)
	zone = models.ForeignKey(Zone, related_name='zonetag')
	tag = models.ForeignKey(Tag, related_name='zonetag')
	date = models.DateTimeField(auto_now=True)

# Categories too?



# from django.contrib.gis.geos import Point
# from django.contrib.gis.geos import GEOSGeometry
# from zones.models import Zone, Tag, ZoneTag

# point = GEOSGeometry('POLYGON((1 1,1 2,2 2,2 1,1 1))')
# # z = Zone(geom=point, colour="FFFFFF")

# point = GEOSGeometry('MULTIPOLYGON((( 10 10, 10 20, 20 20, 20 15, 10 10)))')

# point = GEOSGeometry('MULTIPOLYGON((( 10 10, 10 20, 20 20, 20 15, 10 10)))')
# z = Zone(name="Test 2", geom=point, colour="FFFFFF")
# z.save()
# o = Zone.objects.first()
# o

# point = GEOSGeometry('POINT(52.696361078274485 -0.87890625)')
# point = GEOSGeometry('POLYGON((1 1,1 2,2 2,2 1,1 1))')
# point = GEOSGeometry('MULTIPOLYGON((( 10 10, 10 20, 20 20, 20 15, 10 10)))')

# from django.contrib.gis.geos import Point, GeometryCollection, Polygon, MultiPolygon
# from zones.models import Zone
# z = Zone()
# p1 = Polygon( ((0, 0), (0, 1), (1, 1), (0, 0)) )
# p2 = Point(5, 5)
# p2 = Point(5, 5)
# p3 = Polygon( ((1, 1), (1, 2), (2, 2), (1, 1)) )
# z.name = "Test"
# z.colour = "#FFF"
# z.geom = GeometryCollection((p1, p2, p3))
# z.save()

# # Query for the zone you just created
# zone = Zone.objects.filter(id=1).first()
# # Returns JSON, should have multiple bits
# zone.geom.json

